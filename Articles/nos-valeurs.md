Les valeurs d'AGATAs. 


AGATAs est née d'un but : promouvoir une vision de la société basée sur des valeurs fortes. Elles sont le socle du projet, sa raison d'être, ce qui nous rassemble au dela de l'objet de l'association. Pour faciliter l'arrivée de nouveaux membres dans la communauté AGATAs et s'assurer que nous allons tous dans le même sens, nous avons exprimés ces valeurs par les 7 actions suivantes : 
-Promovoir la Transition écologique et sociale : pour mettre fin à l'extractivisme et au consumérisme qui sacagent la planète et  réduisent l'humanité à l'esclavagisme, nous souhaitons oeuvrer au développement des solutions durables et solidaires. 
-Revendiquer la Justice Environnementale : les inégalités faisant peser sur les plus faibles les dégats environnementaux causés par les plus puissants, nous souhaitons favoriser la réparation des dégats que ces derniers causent, et la réduction des inégalités pour prendre le problème à la racine. 
-Refuser les rentes : toute forme de rente étant une barrière au partage qui alimente les inégalités entre ceux qui ont, et ceux qui n'ont pas, nous basons notre modèle économique sur le travail.
-Promouvoir les communs et le libre : les biens communs favorisant le partage au sein des communautés et garantissant les postures de coopération, nous souhaitons mettre le fruit de notre travail au service du bien commun, sous licence libre. 
-Instaurer une gouvernance partagée et responsable : pour apprendre à coopérer et gérer ensemble les communs, nous souhaitons donner une place à tous ceux qui en bénéficient et promouvoir des comportements basés sur la responsabilisation de tous. 
-Créer un espace sur : pour assurer la participation active de ceux que ce système actuel opprime et infériorise le plus, nous souhaitons créer un espace sur (safe space) où les comportements oppressifs sont conscientisés et mis de côté. 
-Etre solidaire les uns des autres : en attaquant l'ordre établi, nous devons être solidaire les uns des autres pour nous protéger des représailles auxquelles nous risquons de nous exposer. 

Chacun de ses points (et surtout les 3 derniers) mérite des approfondissements, peut etre des clarifications, et surtout des traductions en actes concrets.  Nous n'avons pas encore mis en place d'espace de discussion interne pour aborder ces points importants, mais c'est prévu ! (tiens, en voila une mission pour des volontaires en quête de participation!)
Pour trouver de l'inspiration, rendez vous sur la page Ressources (qu'on va bientot mettre en ligne, si-si !). Certaines références ne sont pas francophones, alors si vous avez des talents de traducteurs, n'hésitez pas. Et si vous avez d'autres références à faire passer, n'hésitez pas non plus à les communiquer ! 



