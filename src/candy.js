var menu = document.getElementById("menu");
var initialOffset = menu.offsetTop;
window.addEventListener("scroll",function(){
	if(menu.offsetTop>4) initialOffset = menu.offsetTop;
	if(window.scrollY > initialOffset){
		menu.className = "fixed";
	}
	else menu.className = "initialPos";
});

var menuItems = document.querySelectorAll("#menu a");
for(var i = 0; i<menuItems.length;i++) menuItems[i].addEventListener("click",function (e) {
		var dest = document.getElementById(e.target.href.split('#')[1]);
		scrollTo(dest.offsetTop, 500);
		document.title = e.target.innerText+" — AGATAs";
		history.pushState(null,document.title,e.target.href);
		e.preventDefault();
		return false;
});
function scrollTo(to, duration) {
	to = Math.min(to,document.body.scrollHeight-window.innerHeight);
	if (window.scrollY == to) return;
	var diff = to - window.scrollY;
	var scrollStep = Math.PI / (duration / 10);
	var count = 0, currPos;
	start = window.scrollY;
	scrollInterval = setInterval(function(){
		if (window.scrollY != to) {
			count = count + 1;
			currPos = start + diff * (0.5 - 0.5 * Math.cos(count * scrollStep));
			document.documentElement.scrollTop = currPos;
			document.body.scrollTop = currPos;
		}
		else { clearInterval(scrollInterval); }
	},10);
}
