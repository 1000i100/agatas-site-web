## Projet de carte

- **Nom de la carte**
- **Problématique**
- **Périmètre de la carte**
- **Commanditaire**

## Etapes de réalisation

- [ ] Définir la problématique de la carte : 
* Définir l'objet de la carte (de quoi s'agit-il ?)
* Définir la problématique-titre de la carte (qu'est ce que la carte va démontrer?)
* Définir l'espace de représentation. 
* Définir l'objectif de la carte (en quoi permet-elle d'agir?)
* Définir le public de la cart.
* Définir l'usage de la carte (pour un collectif, un réseau, un média, destinée à impression ou au partage viral?...) 
- [ ] Description de l'espace
* Définition du fond de carte (géographique, canevas...). 
- [ ] Définition des conventions cartographiques :
* Construction de la légende.
* Si la carte est sociale : définir les forces d'attraction/répulsion. 
* Structuration du formulaire. 
* Choix des pictogrammes. 
* Validation par le commanditaire. 
- [ ] Collecte et choix des informations.
- [ ] Personnalisation graphique et validation par le commanditaire. 
* Adapter identité visuelle. 
* Traduction évuentelle. 
* Validation du projet. 

---
- [ ] validation **technique** par l'équipe (ça fonctionne ?)
- [ ] validation **ergonomique** par l'équipe (c'est pratique ?)
- [ ] validation **editorial** par l'équipe (pas de fautes ? quel que soit la langue ? et les message d'erreurs ?)
- [ ] validation par l'émetteur du besoin

## Contact

- **Demandeur/commanditaire** 
- **{status}** {prénom nom} {email} {téléphone} {autre}
- **{status}** {prénom nom} {email} {téléphone} {autre}
- **{status}** {prénom nom} {email} {téléphone} {autre}

- **Partenaires** 
- **{status}** {prénom nom} {email} {téléphone} {autre}
- **{status}** {prénom nom} {email} {téléphone} {autre}
- **{status}** {prénom nom} {email} {téléphone} {autre}

Exemples de status : *en quelle qualité est-il pertinant de les contacter ?*
- Journaliste
- Militant technophile
- Militant techno-frileux
- Traducteur
