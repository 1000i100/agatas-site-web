lien vers l'article :

## flux éditorial
- [ ] rédaction initiale
- [ ] amélioration collective
- [ ] secrétariat de rédaction
- [ ] publié

### option partage réseaux sociaux
- [ ] partagé sur facebook
- [ ] partagé sur diaspora*
- [ ] partagé sur twitter
- [ ] partagé sur Mastodon

### option multilingue
- [ ] traduction anglais
- [ ] relecture anglais

- [ ] traduction espagnole
- [ ] relecture espagnole

- [ ] traduction espagnole
- [ ] relecture espagnole

- [ ] traduction allemand
- [ ] relecture allemand
